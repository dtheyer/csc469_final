package main
import(
    "fmt"
    "container/heap"
    "time"
)
func node_main(tell [12]chan msg, rank int){
   pq := make(PriorityQueue, 0)
   heap.Init(&pq)
   user_list := make(map[string]int)
   pending_data := 1
   state := 0

   //key = username + filename
   //value = file struct
   data := make(map[string]file)
   data_s1 := make(map[string]file)
   data_s2 := make(map[string]file)
   replica1 := get_secondary(rank, 1)
   replica2 := get_secondary(rank, 2)
   membership := make(chan heartbeat, 100)

   var temp msg

   go maintain_membership(tell, replica1, replica2, membership)

   for state == 0{
      //read data from channel into the queue
      for pending_data == 1 {
         select{
         case new_msg := <-tell[rank]:
            Enqueue(&pq, new_msg)
         default:
            pending_data = 0
         }
      }

      //while the queue isn't empty process the mesages
      for pq.Len() != 0 {
         temp = Dequeue(&pq)
         state, data, data_s1, data_s2, user_list = process_msg(temp, membership, data, data_s1, data_s2, user_list, tell, rank)
	 if state == 1{
	    break
	 }
      }
      pending_data = 1
   }
}

func maintain_membership(tell [12]chan msg, replica1 int, replica2 int, membership chan heartbeat){
   //both replica nodes are assumed to be alive at first
   r1 := true
   r2 := true
   m := make(map[int]int64)
   m[replica1] = -1
   m[replica2] = -1
   for true{
      //stop if all of our members are dead
      if (!r1 && !r2) {
         return
      }

      select{
      case hb := <-membership:
         // If our Node dies, we must die as well
         if hb.update == KILL{
            return
         }

	 //store when we sent the latest ping
	 if hb.update == PING{
	   m[hb.rank] = time.Now().Unix()
	 }

	 //mark ping as being reponded to
	 if hb.update == ACK{
	   m[hb.rank] = -1
	 }
      default:
         //if ping was not responded to within THRESHOLD
         if r1 && m[replica1] > 0 && m[replica1] < (time.Now().Unix() - THRESHOLD){
            fmt.Println( "Node", replica1, "failed to respond to a ping")
	    killNode(tell, replica1) //tell master node
	    r1 = false // mark node as dead internally
         }

         if r2 && m[replica2] > 0 && m[replica2] < (time.Now().Unix() - THRESHOLD){
            fmt.Println("I think", replica2, "is dead!!!!")
	    killNode(tell, replica2) //tell master node
	    r2 = false // mark node as dead internally
         }

         //sleep to save CPU cycles
         time.Sleep(1 * time.Second)
       }
   }
}

//returns 1 if kill was found and 0 otherwise
func process_msg(m msg, membership chan heartbeat, data map[string]file, data_s1 map[string]file, data_s2 map[string]file, user_list map[string]int, tell [12]chan msg, rank int) (int, map[string]file, map[string]file, map[string]file, map[string]int){
   switch m.msg_type {
   case KILL:
      fmt.Println("Nodes all agree", rank, "is probably down. Marking node as offline for in membership list for master",)
      return 1, data, data_s1, data_s2, user_list

   case PING:
	   tell[m.sender] <- msg{sender: rank, msg_type: ACK}

   case ACK:
      //notify membership thread that we got an ACK
      membership <- heartbeat{rank: m.sender, update: ACK}
      return 0, data, data_s1, data_s2, user_list

   case WRITE:
      //if this user belongs to us, store the data and send copy to replica Nodes 
      if user_list[m.f.owner] == PRIMARY{
         data[m.f.owner + m.f.fname] = m.f

	 //tell replica nodes to write the data as well
	 tell[get_secondary(rank, 1)] <- msg{sender: rank, msg_type: WRITE, f: m.f}
	 tell[get_secondary(rank, 2)] <- msg{sender: rank, msg_type: WRITE, f: m.f}

         //send ping inorder to ensure the nodes are alive
	 tell[get_secondary(rank, 1)] <- msg{sender: rank, msg_type: PING}
	 tell[get_secondary(rank, 2)] <- msg{sender: rank, msg_type: PING}
         membership <- heartbeat{rank: get_secondary(rank, 1), update: PING}
         membership <- heartbeat{rank: get_secondary(rank, 2), update: PING}
      }else

      //if this write request is from a user in the node we are replicating
      if (user_list[m.f.owner] == SECONDARY1) && (m.sender != CONSOLE){
         data_s1[m.f.owner + m.f.fname] = m.f
      }else
      if (user_list[m.f.owner] == SECONDARY2) && (m.sender != CONSOLE){
         data_s2[m.f.owner + m.f.fname] = m.f
      }else{
	      fmt.Println("User", m.f.owner, "was not found on server", rank, "-- relaying write to master node")
         tell[MASTER] <- m
      }

      //else forward request to the master node
      return 0, data, data_s1, data_s2, user_list

   case READ:
      //if the user is ours, send the file to the console
      if user_list[m.f.owner] == PRIMARY{
	      fmt.Print(m.f.fname, " (size: ", data[m.f.owner+m.f.fname].size,  ", Owner: ", m.f.owner,") was retreived from node ", rank, "\n")
         return 0, data, data_s1, data_s2, user_list
      }
      //else ask the master
      tell[MASTER] <- m

   case EDIT_USER:
      //If the Mesage is from the Master -> Primary
      if m.sender == MASTER{
         if m.f.id == -1 {
            delete(user_list, m.f.owner) //remove user from node is they were aleady there
         }else{
	    fmt.Println("User", m.f.owner, "was assigned to Node", rank);
            user_list[m.f.owner] = PRIMARY // Add user if they weren't
         }
	 //tell the nodes replicating us to add/remove this user as well
	 tell[get_secondary(rank, 1)] <- msg{sender: rank, msg_type: EDIT_USER, f: m.f}
	 tell[get_secondary(rank, 2)] <- msg{sender: rank, msg_type: EDIT_USER, f: m.f}
         return 0, data, data_s1, data_s2, user_list
      }
      //If the mesage is from the Primary -> Replica Node
      if m.f.id == -1 {//&& user_list[m.f.owner] != PRIMARY{
         delete(user_list, m.f.owner) //remove user from node is they were aleady there
      }else{
        user_list[m.f.owner] = secondary_from_primary(rank, m.sender) // Add user if they weren't
      }

   case REBALANCE:
       //if m.f.id == -1
       // then m.sender is the primary node that has failed
      //copy user from secondary set to primary set
      //NOTE data is not deleted from replica to allow for data replication in the case of
      //multi-node failure
      if m.f.id == -1{
	 fmt.Println("Attempting to convert", m.f.owner, "from secondary to primary on rank", rank)
         if user_list[m.f.owner] == SECONDARY1{
            for _, file1 := range data_s1{
	       if file1.owner == m.f.owner{
	          data[file1.owner + file1.fname] = file1
	       }
	    }
	 }else if user_list[m.f.owner] == SECONDARY2{
            for _, file1 := range data_s1{
	       if file1.owner == m.f.owner{
	          data[file1.owner + file1.fname] = file1
	       }
	    }
	 }
	 //update user_list to contain user in PRIMARY dataset
	 user_list[m.f.owner] = PRIMARY

	 //Update old secondaries by telling them to drop the user
         tell[get_secondary(m.sender, 1)] <- msg{sender: MASTER, msg_type: EDIT_USER, f: file{owner: m.f.owner, id: -1}}
         tell[get_secondary(m.sender, 2)] <- msg{sender: MASTER, msg_type: EDIT_USER, f: file{owner: m.f.owner, id: -1}}

	 //tell new secondaries to add the user
	 tell[rank] <- msg{sender: MASTER, msg_type: EDIT_USER, f: file{owner: m.f.owner, id: 1}}
	 //time.Sleep(5 * time.Second)
         return 0, data, data_s1, data_s2, user_list
      }
      fmt.Println("Attempting to move", m.f.owner, "from", rank, "to", m.f.id)
      //CASE of moving Primary data to a new Node
      //m.f.id represents the target node that will be the new primary


      //tell secondaries they no longer need to replicate this user
      tell[get_secondary(rank, 1)] <- msg{sender: rank, msg_type: EDIT_USER, f: file{owner: m.f.owner, id: -1}}
      tell[get_secondary(rank, 2)] <- msg{sender: rank, msg_type: EDIT_USER, f: file{owner: m.f.owner, id: -1}}

      //tell new primary node to add the user
      tell[m.f.id] <- msg{sender: MASTER, msg_type: EDIT_USER, f: file{owner: m.f.owner, id: 1}}
      //time.Sleep(5 * time.Second)
      //copy all files belonging to that user to the new primary node
      for _, file1 := range data{
	      if file1.owner == m.f.owner{
	         tell[m.f.id] <- msg{sender: rank, msg_type: WRITE, f: file1}
              }
      }

      //remove user from user_list
      delete(user_list, m.f.owner)
   }
      return 0, data, data_s1, data_s2, user_list
}
//given the rank of the primary, returns the nodes rank that corresponds to its secondry
//secondary number must be either 1 or 2
func get_secondary(rank int, secondary_number int) int{
  if secondary_number == 1{
    if rank == NUM_NODES + 1{
       return 2
    }
    return rank +1
  }

  if secondary_number == 2{
    if rank >= NUM_NODES{
       return rank - 8
    }
    return rank + 2
  }
  //function was used incorrectly
  return -1
}

//determine which secondary the primary relates to
// the secondaty rank calls this to findout if the primary needing data
// relates to its secondary1 or secondary2 storage sector
func secondary_from_primary(my_rank int, sender_rank int) int{
   if (sender_rank > my_rank){ //rollover occured
      if sender_rank == my_rank + 8{
         return 2
      }
      return 1
   }
   return my_rank - sender_rank

}

