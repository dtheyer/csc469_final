package main

import(
   "fmt"


)
func master_main(tell [12]chan msg, rank int){
   pq := make(PriorityQueue, 0)
   user_list := make(map[string]int) //user -> node
   pending_data := 1
   nodes := [12]int{1,1,1,1,1,1,1,1,1,1,1,1}
   var backup int
   //var m msg

   //go maintain_membership(tell, replica1, replica2, membership)

   for true{
      //read data from channel into the queue
      for pending_data == 1 {
         select{
         case new_msg := <-tell[rank]:
            Enqueue(&pq, new_msg)
         default:
            pending_data = 0
         }
      }

      //while the queue isn't empty process the mesages
      for pq.Len() != 0 {
         m := Dequeue(&pq)
         switch m.msg_type {
         //------------------------------------------------------------------------------------------
         //represents a Node notifying the master that one of it's members is dead
         case KILL:
            //mark node as being down 
            nodes[m.sender] = 0

	    //determine if either of the replica nodes are still alive
            if nodes[get_secondary(m.sender, 1)] == 1 {
	       backup = get_secondary(m.sender, 1)
	    } else if nodes[get_secondary(m.sender, 2)] == 1 {
	       backup = get_secondary(m.sender, 2)
	    } else {
	       fmt.Println(m.sender, "and both replica nodes have gone down. Data recovery is not possible")
	    }
	    //We need to assign the users at that node a new primary and recover their 
	    //data from one of the replica Nodes
	    for user, node := range user_list{
	       //for each user in the node that failed
	       if(node == m.sender){
		       //tell replica note to become the primary node for that use
		       fmt.Println("Recovered user", user)
		       tell[backup] <-msg{sender: m.sender, msg_type: REBALANCE, f: file{owner: user, id: -1}}
		       user_list[user] = backup
	       }
	    }
	    fmt.Println(backup, "has taken over for failed node:", m.sender)

	 //-----------------------------------------------------------------------------------------  
	 case EDIT_USER: //allocate this new user a primary Node
            temp := m.sender
	    for nodes[temp] == 0{
	       temp++
	       if temp == 12{
	          temp = 2
	       }
	       if temp == m.sender{
	          fmt.Println("All Nodes are Dead....Bailing")
		  return
	       }
	    }
	    //add user to that closest clockwise node that is still alive
	    user_list[m.f.owner] = temp
            tell[temp] <- msg{sender:  MASTER, msg_type: EDIT_USER, f: file{owner: m.f.owner, size: -1, id: m.f.id, fname: "nil"}}

	 //-----------------------------------------------------------------------------------------
	 case WRITE: //redirect write to the primary Node of the user
	    tell[user_list[m.f.owner]] <- m
	    fmt.Println("Master redirected the write to node", user_list[m.f.owner])

	 case READ:  //redirect READ to the primary Node
	    tell[user_list[m.f.owner]] <- m
	    fmt.Println("Master redirected the read to node", user_list[m.f.owner])

	 case REBALANCE: //move user manualy
	    tell[m.f.id] <-msg{sender: MASTER, msg_type: REBALANCE, f: file{owner: m.f.owner, size: -1, id: m.sender}}
	    user_list[m.f.owner] = m.sender
	 }

      }
      pending_data = 1
   }
}

