# Names:
David Heyer<br>
Dominique Sayo<br>
Micheal McAniff<br>

## compiling
go run *.go

## example input
Example inputs are provided in sample_input.txt.
the lines can be pasted into the parser manually or input using unix redirects (though the parser may crash on the last line due to the OS the program is running on)
