package main

import (
	"container/heap"
)

type Item struct {
   Message msg
   Priority int          //The priority of the message
   index int
}

// A PriorityQueue implements heap.Interface and holds Items.
type PriorityQueue []*Item

func (pq PriorityQueue) Len() int { return len(pq) }

func (pq PriorityQueue) Less(i, j int) bool {
	return pq[i].Priority > pq[j].Priority
}

func (pq PriorityQueue) Swap(i, j int) {
	pq[i], pq[j] = pq[j], pq[i]
	pq[i].index = i
	pq[j].index = j
}

func (pq *PriorityQueue) Push(x interface{}) {
	n := len(*pq)
	item := x.(*Item)
	item.index = n
	*pq = append(*pq, item)
}

func (pq *PriorityQueue) Pop() interface{} {
	old := *pq
	n := len(old)

	item := old[n-1]
	item.index = -1 // for safety
	*pq = old[0 : n-1]
	return item
}

func Enqueue(pq *PriorityQueue, message msg)  {
   item := &Item{
	   Message: message,
           Priority: message.msg_type,
   }
   heap.Push(pq, item)
}

func Dequeue(pq *PriorityQueue) msg {
   msg := heap.Pop(pq).(*Item)
   return msg.Message
}

//func Enqueue(queue []msg, message msg) []msg{
//   return append(queue, message)
//}

//func Dequeue(queue []msg)  ([]msg, msg) {
//   return queue[1:], queue[0]
//}

