package main

import (
      "fmt"
      "time"
      "bufio"
      "os"
      "strings"
      "strconv"
      //"math/rand"
)
const NUM_NODES = 12

const MASTER  = 1
const CONSOLE = 0

const PRIMARY = 3
const SECONDARY1 = 1
const SECONDARY2 = 2

const KILL       = 0
const PING       = 1
const ACK        = 2
const REBALANCE  = 3
const EDIT_USER  = 4
const WRITE      = 5
const READ       = 6

const THRESHOLD = 5

type heartbeat struct {
   rank int
   time int64
   update int
}

type file struct {
   owner string  //the owner of the file
   size int      //size of the file in MB
   id int
   fname string  //fname string
}

type msg struct {
   sender int    // the rank of the node who sent the message
   msg_type int  // the type of mesage
   f file
}

func main(){

   //An array of channels for the Nodes to communicate through
   var tell [12]chan msg

   //init buffered channels
   for  i:=0; i<12; i++ {
       tell[i] = make(chan msg, 1000)
   }

   go master_main(tell, MASTER)

   //launch server nodes
   for i:=2; i<NUM_NODES; i++{
       go node_main(tell, i)
   }

   reader := bufio.NewReader(os.Stdin)
   fmt.Println("\n\nDistributed File System")
   fmt.Println("---------------------")
   fmt.Println("\nUsage:")
   fmt.Println("\t(Nodes and locations must be integer values from 2-11)")
   fmt.Println("\tadduser 'username' 'location'")
   fmt.Println("\tread 'username' 'filename' 'location'")
   fmt.Println("\twrite 'username' 'filename' 'size' 'location'")
   fmt.Println("\tmoveuser 'username' 'old location' 'new location'")
   fmt.Println("\tkillnode 'node'")

  for{
      //delay added to improve readability of output
      time.Sleep(2 * time.Second)
      fmt.Print("-> ")
      text, _ := reader.ReadString('\n')

      // convert CRLF to LF
      text = strings.Replace(text, "\n", "", -1)
      words := strings.Fields(text)
      switch words[0] {
      case "adduser":
         if (len(words) != 3) {
            printErrorMessage()
	    continue
         }
         username := words[1]
         location,_ := strconv.Atoi(words[2])
	 addUser(tell, username, location)

      case "read":
         if (len(words) != 4) {
            printErrorMessage()
	    continue
         }
         username := words[1]
         filename := words[2]
         location,_ := strconv.Atoi(words[3])
	 readFile(tell, username, filename, location)

      case "write":
         if (len(words) != 5) {
            printErrorMessage()
	    continue
         }
         username := words[1]
         filename := words[2]
         size,_ := strconv.Atoi(words[3])
         location,_ := strconv.Atoi(words[4])
	 writeFile(tell, username, filename, size, location)
         fmt.Println("File has been written")
      case "moveuser":
         if (len(words) != 4) {
            printErrorMessage()
	    continue
         }
         username := words[1]
         old_location,_ := strconv.Atoi(words[2])
         new_location,_ := strconv.Atoi(words[3])
	 moveUser(tell, username, old_location, new_location)

      case "killnode":
         if (len(words) != 2) {
            printErrorMessage()
	    continue
         }
         dead_beef,_ := strconv.Atoi(words[1])
	 killNode(tell, dead_beef)

    default:
       fmt.Println("Seems something went wrong, try again\n")
    }
   }
}

//notify the master of a move user request
func moveUser(tell [12]chan msg, user string, old_primary int, new_primary int){
	tell[MASTER] <-msg{sender: new_primary, msg_type: REBALANCE, f: file{owner: user, size: -1, id: old_primary}}
}

//read a read request to the closest node
func readFile(tell [12]chan msg, user string, filename string, target int){
	tell[target] <- msg{sender: CONSOLE, msg_type: READ, f: file{owner: user, size: -1, id: 0, fname: filename}}
}

//send a write request to the closest node
func writeFile(tell [12]chan msg, user string, filename string, file_size int, primary int){
   tell[primary] <- msg{sender: CONSOLE, msg_type: WRITE, f: file{owner: user, size: file_size, id: 0, fname: filename}}

}

//ask the master to assign a primary node to this new user
func addUser(tell [12]chan msg, user string, location int){
   tell[MASTER] <- msg{sender:  location, msg_type: EDIT_USER, f: file{owner: user, size: -1, id: 1, fname: "nil"}}
   fmt.Println("message was sent to master node")
}

//kill a node (simulate it going down)
func killNode(tell [12]chan msg, target int){
	tell[target] <-msg{sender: CONSOLE, msg_type: KILL}
	tell[1] <- msg{sender: target}
}

func printErrorMessage() {
   fmt.Println("Error - Could not read input, please try again\n")
}


